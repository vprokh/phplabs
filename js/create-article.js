export default class CreateArticle {
    constructor() {
        this.nextBtns = $('.nextBtn');
        this.contents = $('.setup-content');
        this.createArticleBtn = $('#createArticle');
        this.navBtns = $('div.setup-panel div a');
        this.createArticleForm = $('#createArticleForm');
        this.submitError = $('#submitError');
    }

    initListeners() {
        this.hideContents();
        this.navBtns.on('click', this.navListItemsClicked.bind(this));
        this.nextBtns.on('click', this.nextBtnClicked.bind(this));
        this.createArticleBtn.on('click', this.createArticleClicked.bind(this));
        this.triggerFirstStep();
    }

    triggerFirstStep() {
        $('#firstStep').trigger('click');
    }

    hideContents() {
        this.contents.hide();
    }

    navListItemsClicked(event) {
        event.preventDefault();
        let target = $(event.target);
        if (target.hasClass('disabled')) {
            return;
        }

        let targetContent = $(target.attr('href'));
        this.hideContents();
        this.navBtns.removeClass('clicked');
        target.addClass('clicked');
        targetContent.show();
        targetContent.find('input:eq(0)').focus();
    }

    nextBtnClicked(event) {
        event.preventDefault();
        let currentStep = $(event.target).closest(".setup-content");
        let currentStepBtn = currentStep.attr("id");
        let nextStepWizard = $('div.setup-panel div a[href="#' + currentStepBtn + '"]').parent().next().children("a");
        let currentInputs = currentStep.find("input[type='text']");
        let textAreas = currentStep.find('textarea');
        let isValid = CreateArticle.validateData(currentInputs, textAreas);

        if (isValid) {
            nextStepWizard.removeClass('disabled').trigger('click');
        }
    }

    createArticleClicked(event) {
        event.preventDefault();
        this.submitError.hide();
        this.submitError.html("");
        let currentInputs = this.contents.find("input[type='text'], input[type='file']");
        let textAreas = this.contents.find('textarea');

        if (CreateArticle.validateData(currentInputs, textAreas)) {
            this.createArticleForm.submit();
        } else {
            this.submitError.html("There some error(s). Please review all steps and try again");
        }
    }


    static validateData(inputs, textAreas) {
        let isValid = true;

        $(".form-group").removeClass("has-error");
        for (let i = 0; i < inputs.length; i++) {
            if (!inputs[i].validity.valid) {
                isValid = false;
                $(inputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        for (let i = 0; i < textAreas.length; i++) {
            if (!textAreas[i].value) {
                isValid = false;
                $(textAreas[i]).closest(".form-group").addClass("has-error");
            }
        }

        return isValid;
    }
}
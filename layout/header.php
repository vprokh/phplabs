<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BlogPoster</title>
    <link rel="stylesheet" href="../css/lib/bootstrap.min.css">
    <link rel="stylesheet" href="../css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="../css/headerStylesheet.css">
    <link rel="stylesheet" href="../css/postList.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" href="../css/about.css">
    <link rel="stylesheet" href="../css/registration.css">
    <link rel="stylesheet" href="../css/createArticle.css">
    <link rel="stylesheet" href="../css/singlePost.css">
    <script src="../js/lib/jquery-3.3.1.min.js"></script>
    <script src="../js/lib/popper-1.14.3.min.js"></script>
    <script src="../js/lib/bootstrap.min.js"></script>
    <script type="module" src="../js/self-invoked.js"></script>
</head>
<body>
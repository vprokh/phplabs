<div id="deleteArticleModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="form-group form-group-condensed">
                    <div>
                        <h3 class="text-center">Are you really wanna delete article?</h3>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">
                    Close
                </button>
                <a class="btn btn-danger pull-right" id="deleteArticleSubmit">
                    Delete
                </a>
            </div>
        </div>
    </div>
</div>
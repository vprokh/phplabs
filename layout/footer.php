        <footer class="footer">
            <div class="container-fluid">
                <ul class="foote_bottom_ul_amrc">
                    <li>
                        <a href="index.php">Home</a>
                        <a href="index.php?action=about">About</a>
                    </li>
                </ul>
                <p class="text-center">All rights reserved @2018 | By <a href="#">Vlad Prokhitskyi</a></p>

                <ul class="social_footer_ul">
                    <li><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
                    <li><a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>
<?php
function upload_article_image()
{
    if (isset($_FILES['articleImage'])) {
        $file_name = uniqid();
        $file_size = $_FILES['articleImage']['size'];
        $file_tmp = $_FILES['articleImage']['tmp_name'];
        $file_ext = strtolower(end(explode('.', $_FILES['articleImage']['name'])));

        $extensions = array("jpeg", "jpg", "png");

        if (in_array($file_ext, $extensions) === false || $file_size > 2097152) {
            return "";
        }

        move_uploaded_file($file_tmp, "./uploadedImage/" . $file_name . '.' . $file_ext);

        return "./uploadedImage/" . $file_name . '.' . $file_ext;
    }
}
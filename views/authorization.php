<?php
if (array_key_exists('userId', $_SESSION)) {
    header('Location: index.php?action=main');
    die();
}
include "script/data_base.php";

$login = "";
$emptyLoginError = "";
$emptyPasswordError = "";
$authorizationError = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (is_fields_not_empty($emptyLoginError, $emptyPasswordError)) {
        $login = $_POST["login"];
        $authorizedUser = get_authorized_user($login, $authorizationError);

        if (is_credentials_correct($authorizedUser, $_POST["password"])) {
            save_to_session($authorizedUser);
            header('Location: index.php?action=main');
        } else if (strlen($authorizationError) == 0) {
            $authorizationError = "Either login or password is incorrect";
        }
    }
}

function is_fields_not_empty(&$emptyLoginError, &$emptyPasswordError) {
    $isFieldsNotEmpty = true;

    if ($_POST["login"] == "") {
        $emptyLoginError = "Login cannot be empty";
        $isFieldsNotEmpty = false;
    }
    if ($_POST["password"] == "") {
        $emptyPasswordError = "Password cannot be empty";
        $isFieldsNotEmpty = false;
    }

    return $isFieldsNotEmpty;
}

function get_authorized_user($login, &$authorizationError) {
    $dbConnection = get_db_connection();
    $statement = $dbConnection->prepare("SELECT u.id, u.login, u.password, u.admin FROM users u WHERE u.login = ?");

    if ($statement) {
        $statement->bind_param("s", $login);
        $statement->execute();
        $result = $statement->get_result();

        return $result->fetch_assoc();
    }

    $authorizationError = "Something went wrong. Please try again later";

    return null;
}

function is_credentials_correct($user, $formPassword) {
    if ($user == false) {
        return false;
    }

    return password_verify($formPassword, $user["password"]);
}

function save_to_session($user) {
    $_SESSION["userId"] = $user["id"];
    $_SESSION["userLogin"] = $user["login"];
    $_SESSION["userAdmin"] = $user["admin"];
}

include "layout/login_form.php";